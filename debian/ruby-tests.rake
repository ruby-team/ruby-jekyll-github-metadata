require 'gem2deb/rake/spectask'

skip = [
  './spec/integration_spec.rb',
  './spec/metadata_drop_spec.rb',
  './spec/site_github_munger_spec.rb',
  './spec/owner_spec.rb',
  './spec/client_spec.rb',
  './spec/repository_spec.rb'
]

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  # exclude tests requiring a git repository
  spec.exclude_pattern = skip.join(',')
end
